---
title: Container Orchestration
nav_order: 10
parent: DevOps Tools Engineer Exam
---

# 7. Container Orchestration

- [ ]  [Introduction to Container Orchestration](https://www.lpi.org/blog/2018/02/20/devops-tools-introduction-07-container-orchestration)
- [ ]  [LPI Objective 702.2](https://wiki.lpi.org/wiki/DevOps_Tools_Engineer_Objectives_V1#702.2_Container_Deployment_and_Orchestration_.28weight:_5.29)
- [ ]  [Container Orchestration with Docker with Swarm](https://container.training/swarm-selfpaced.yml.html#1)
- [ ]  [Play with Docker](https://labs.play-with-docker.com/)
- [ ]  [Learn Kubernetes Basics](https://kubernetes.io/docs/tutorials/kubernetes-basics/)
- [ ]  [Play with Kubernetes](https://labs.play-with-k8s.com/)
- [ ]  [Picking the Right Kubernetes Solution ](https://kubernetes.io/docs/setup/pick-right-solution/#hosted-solutions)
- [ ]  [Install Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
- [ ]  [Kubernetes 101](https://container.training/kube-halfday.yml.html#1)
- [ ]  [Pod Overview](https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/)
- [ ]  [Pods in Kubernetes](https://kubernetes.io/docs/concepts/workloads/pods/pod/)
- [ ]  [Pod Lifecycle in Kubernetes](https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/)
- [ ]  [Deployments in Kubernetes](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
- [ ]  [ReplicaSet in Kubernetes](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/)
- [ ]  [Services in Kubernetes](https://kubernetes.io/docs/concepts/services-networking/service/)
- [ ]  [Tasks in Kubernetes](https://kubernetes.io/docs/tasks/)
