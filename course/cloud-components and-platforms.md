---
title: Cloud Components and Platforms
nav_order: 6
parent: DevOps Tools Engineer Exam
---

# 3. Cloud Components and Platforms

- [ ]  [Introduction to Cloud Components and Platforms](https://www.lpi.org/blog/2018/01/23/devops-tools-introduction-03-cloud-components-and-platforms)
- [ ]  [LPI Objective 701.2](https://wiki.lpi.org/wiki/DevOps_Tools_Engineer_Objectives_V1#701.2_Standard_Components_and_Platforms_for_Software_.28weight:_2.29)
- [ ]  [OpenStack Components](https://www.openstack.org/software/project-navigator/openstack-components#openstack-services)
- [ ]  [Cloud Foundry](https://www.cloudfoundry.org/why-cloud-foundry/)
- [ ]  [Red Hat OpenShift](https://www.openshift.com/products/features/)
- [ ]  [OpenShift Tutorial](https://www.tutorialspoint.com/openshift/index.htm)
- [ ]  [OpenShift Playgrounds](https://www.katacoda.com/mrhillsman/courses/playgrounds)
- [ ]  [OpenStack Swift](https://docs.openstack.org/swift/latest/)
- [ ]  [OpenStack Trove](https://docs.openstack.org/trove/latest/)
- [ ]  [OpenStack Zaqar](https://docs.openstack.org/zaqar/latest/)
