---
title: Container Infrastructure
nav_order: 12
parent: DevOps Tools Engineer Exam
---

# 8. Container Infrastructure

- [ ]  [Introduction to Container Infrastructure](https://www.lpi.org/blog/2018/02/27/devops-tools-introduction-08-container-infrastructure)
- [ ]  [LPI Objective 702.3](https://wiki.lpi.org/wiki/DevOps_Tools_Engineer_Objectives_V1#702.3_Container_Infrastructure_.28weight:_4.29)  
- [ ]  [Docker Desktop Overview](https://docs.docker.com/desktop/)
- [ ]  [Install Docker Desktop on Windows](https://docs.docker.com/desktop/windows/install/)
- [ ]  [Install Docker Desktop on Mac](https://docs.docker.com/desktop/mac/install/)
- [ ]  [Networking overview](https://docs.docker.com/network/)
- [ ]  [Networking with overlay networks](https://docs.docker.com/network/network-tutorial-overlay/)
- [ ]  [Bridge network tutorial](https://docs.docker.com/network/network-tutorial-standalone/)
- [ ]  [Use bind mounts](https://docs.docker.com/storage/bind-mounts/)
- [ ]  [Docker storage drivers](https://docs.docker.com/storage/storagedriver/select-storage-driver/)
- [ ]  [About storage drivers](https://docs.docker.com/storage/storagedriver/)
- [ ]  [ClusterHQ/flocker: Container data volume manager for your Docker](https://github.com/ClusterHQ/flocker)
- [ ]  [coreos/flannel: flannel is a network fabric for containers, designe for Kubernetes](https://github.com/coreos/flannel)
- [ ]  [CoreOS Container Linux Documentation](https://coreos.com/os/docs/latest/)
- [ ]  [rkt, a security-minded, standards-based container engine](https://coreos.com/rkt/)
- [ ]  [Using etcd](https://coreos.com/etcd/)
- [ ]  [Service Discovery with Consul](https://sreeninet.wordpress.com/2016/04/17/service-discovery-with-consul/)
- [ ]  [Docker security](https://docs.docker.com/engine/security/security/)
- [ ]  [10 layers of Linux container security](https://opensource.com/article/17/10/10-layers-container-security)
- [ ]  [5 tips for securing your Docker containers](https://www.techrepublic.com/article/5-tips-for-securing-your-docker-containers/)
- [ ]  [Assessing the Current State of Container Security](https://thenewstack.io/assessing-the-state-current-container-security/)
- [ ]  [The Ultimate Guide to Container Security](https://techspective.net/2017/07/20/ultimate-guide-container-security/)
