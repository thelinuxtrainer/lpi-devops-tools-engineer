---
title: DevOps Tools Engineer Exam
nav_order: 2
has_children: true
---

# LPI DevOps Tools Engineer Exam Study Resources

The links for studying [Exam 701: DevOps Tools Engineer](https://www.lpi.org/our-certifications/exam-701-objectives) offered here are found from the LPI blog. When you've done studying a topic, check the checkbox next to it. This website was inspired by [Hamidreza Josheghani's](https://github.com/pyguy) great work, which you caan find at [LPI DevOps study resources](https://github.com/pyguy/lpi-devops-study). This website, which I have designed for you, has up-to-date information as of February 2022, as well as training modules and labs that I have designed for exam practice and real-world skill development.

[I](https://www.linkedin.com/in/chinthakadeshapriya/) wish you  Good luck on your [LPI DevOps Tools Engineer Exam](https://www.lpi.org/our-certifications/exam-701-objectives).

# List of Study Resources

* [Getting Started](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#GettingStarted)
* [Modern Software Development](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#ModernSoftwareDevelopment)
* [Cloud Components and Platforms](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#CloudComponentsandPtforms)
* [Source Code Management](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#SourceCodeManagement)
* [Continuous Delivery](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#ContinuousDelivery)
* [Container Basics](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#ContainerBasics)
* [Container Orchestration](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#ContainerOrchestration)
* [Container Infrastructure](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#ContainerInfrastructure)
* [Machine Deployment](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#MachineDeployment)
* [Ansible](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#Ansible)
* [Other Configuration Management Tools](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#OtherConfigurationManagementTools)
* [IT Operations and Monitoring](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#ITOperationsandMonitoring)
* [Log Management and Analysis](https://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#LogManagementandAnalysis)
* [Taking the Test](thttps://thelinuxtrainer.gitlab.io/lpi-learning-resources/course/table-of-content/#TakingtheTest)
* [Books](Books.md)
