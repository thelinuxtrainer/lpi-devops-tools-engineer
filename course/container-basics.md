---
title: Container Basics
nav_order: 9
parent: DevOps Tools Engineer Exam
---

# 6. Container Basics

- [ ]  [Introduction to Container Basics](https://www.lpi.org/blog/2018/02/13/devops-tools-introduction-06-container-basics)
- [ ]  [LPI Objective 702.1](https://wiki.lpi.org/wiki/DevOps_Tools_Engineer_Objectives_V1#702.1_Container_Usage_.28weight:_7.29)
- [ ]  [Play with Docker Classroom](https://training.play-with-docker.com/)
- [ ]  [Container Training](https://container.training/)
- [ ]  [Docker for IT Pros and System Administrators](https://training.play-with-docker.com/ops-landing/)
- [ ]  [Introduction to Containers](https://container.training/intro-fullday.yml.html#1)
- [ ]  [Docker overview](https://docs.docker.com/engine/docker-overview/)
- [ ]  [Container Networking](https://docs.docker.com/engine/tutorials/networkingcontainers/)
- [ ]  [Manage data in Docker](https://docs.docker.com/storage/)
- [ ]  [Use Storage volumes](https://docs.docker.com/storage/volumes/)
- [ ]  [Dockerfile](https://docs.docker.com/engine/reference/builder/)
- [ ]  [Best practices for writing Dockerfiles](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
- [ ]  [docker build command](https://docs.docker.com/engine/reference/commandline/build/)
- [ ]  [Using the Docker command line](https://docs.docker.com/engine/reference/commandline/cli/)
- [ ]  [.dockerignore file](https://docs.docker.com/engine/reference/builder/#dockerignore-file)
