---
title: Modern Software Development
nav_order: 5
parent: DevOps Tools Engineer Exam
---

# 2. Modern Software Development

- [ ]  [Introduction to Modern Software Development](https://www.lpi.org/blog/2018/01/16/devops-tools-introduction-02-modern-software-development)
- [ ] [LPI Objective 701.1](https://wiki.lpi.org/wiki/DevOps_Tools_Engineer_Objectives_V1#701.1_Modern_Software_Development_.28weight:_6.29)
- [ ]  [The Twelve-Factor App](https://12factor.net/) 
- [ ]  [Martin Fowler](https://martinfowler.com/)
- [ ]  [EnterpriseApplication](https://martinfowler.com/bliki/EnterpriseApplication.html) 
- [ ]  [Microservices](https://martinfowler.com/articles/microservices.html)
- [ ]  [ImmutableServer](https://martinfowler.com/bliki/ImmutableServer.html)
- [ ]  [Don't start with a monolith](https://martinfowler.com/articles/dont-start-monolith.html)
- [ ]  [An Introduction to API's - The RESTful Web](https://restful.io/an-introduction-to-api-s-cee90581ca1b)
- [ ]  [JSON:API - A specification for building APIs in JSON](https://jsonapi.org/)
- [ ]  [OWASP](https://www.owasp.org/index.php/Main_Page)
- [ ]  [Cross-site Scripting (XSS) - OWASP](https://www.owasp.org/index.php/Cross-site_Scripting_(XSS))
- [ ]  [SQL Injection - OWASP](https://www.owasp.org/index.php/SQL_Injection)
- [ ]  [Web Service Security Cheat Sheet - OWASP](https://www.owasp.org/index.php/Web_Service_Security_Cheat_Sheet)
- [ ]  [Authentication Cheat Sheet - OWASP](https://www.owasp.org/index.php/Authentication_Cheat_Sheet)
- [ ]  [Transport Layer Protection Cheat Sheet - OWASP](https://www.owasp.org/index.php/Transport_Layer_Protection_Cheat_Sheet)
- [ ]  [Cross-Site Request Forgery (CSRF) - OWASP](https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF))
- [ ]  [Error Handling, Auditing and Logging - OWASP](https://www.owasp.org/index.php/Error_Handling,_Auditing_and_Logging)
- [ ]  [OWASP Cheat Sheet Series - OWASP](https://www.owasp.org/index.php/OWASP_Cheat_Sheet_Series)
- [ ]  [What is the relation between SQL, NoSQL, the CAP theorem and ACID? - Quora](https://www.quora.com/What-is-the-relation-between-SQL-NoSQL-the-CAP-theorem-and-ACID)
- [ ]  [Significance of ACID vs BASE vs CAP Philosophy in Data Science](https://medium.com/analytics-vidhya/significance-of-acid-vs-base-vs-cap-philosophy-in-data-science-2cd1f78200ce)
- [ ]  [Manifesto for Agile Software Development](http://agilemanifesto.org/)
- [ ]  [Agile 101](https://www.agilealliance.org/agile101/)
- [ ]  [The Agile Fluency Model](https://martinfowler.com/articles/agileFluency.html)
- [ ]  [Agile Guide](https://martinfowler.com/agile.html)
- [ ]  [DevOps Culture](https://martinfowler.com/bliki/DevOpsCulture.html)
- [ ]  [Certified Agile Service Manager (CASM)®](https://www.devopsinstitute.com/certifications/certified-agile-service-manager/)
