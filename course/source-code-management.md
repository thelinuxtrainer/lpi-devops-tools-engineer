---
title: Source Code Management
nav_order: 7
parent: DevOps Tools Engineer Exam
---

# 4. Source Code Management

- [ ]  [Introduction to Source Code Management](https://www.lpi.org/blog/2018/01/30/devops-tools-introduction-04-source-code-management)
- [ ]  [LPI Objective 701.3](https://wiki.lpi.org/wiki/DevOps_Tools_Engineer_Objectives_V1#701.3_Source_Code_Management_.28weight:_5.29)
- [ ]  [Resources to learn Git](http://try.github.io/)
- [ ]  [Git - Book](https://git-scm.com/book/en/v2)
- [ ]  [Git - Tagging](https://git-scm.com/book/en/v2/Git-Basics-Tagging)
- [ ]  [Git - Branches in a
Nutshell](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)
- [ ]  [Git - Basic Branching and
Merging](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
- [ ]  [Git - Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
- [ ]  [Git - Revision
Selection](https://git-scm.com/book/en/v2/Git-Tools-Revision-Selection)
- [ ]  [Git - Setup and
Config](https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Setup-and-Config)
- [ ]  [GIT vs SVN - CodeForest](https://www.codeforest.net/git-vs-svn)
- [ ]  [VersionControlTools](https://martinfowler.com/bliki/VersionControlTools.html)
