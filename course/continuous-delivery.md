---
title: Continuous Delivery
nav_order: 8
parent: DevOps Tools Engineer Exam
---

# 5. Continuous Delivery 

- [ ]  [Introduction to Continuous Delivery](https://www.lpi.org/blog/2018/02/06/devops-tools-introduction-05-continuous-delivery)
- [ ]  [LPI Objective 701.4](https://wiki.lpi.org/wiki/DevOps_Tools_Engineer_Objectives_V1#701.4_Continuous_Integration_and_Continuous_Delivery_.28weight:_5.29) 
- [ ]  [Continuous Delivery](https://martinfowler.com/bliki/ContinuousDelivery.html)
- [ ]  [Continuous Integration](https://martinfowler.com/articles/continuousIntegration.html)
- [ ]  [Deployment Pipeline](https://martinfowler.com/bliki/DeploymentPipeline.html)
- [ ]  [Canary Release](https://martinfowler.com/bliki/CanaryRelease.html)
- [ ]  [BlueGreen Deployment](https://martinfowler.com/bliki/BlueGreenDeployment.html)
- [ ]  [Blue-green Deployments, A/B Testing, and Canary Releases - Software Blog](http://blog.christianposta.com/deploy/blue-green-deployments-a-b-testing-and-canary-releases/)
- [ ]  [Getting started with the Jenkins Guided Tour](https://jenkins.io/doc/pipeline/tour/getting-started/)
- [ ]  [Using a Jenkins file](https://jenkins.io/doc/book/pipeline/jenkinsfile/)
- [ ]  [Pipeline Syntax](https://jenkins.io/doc/book/pipeline/syntax/)
- [ ]  [Jenkins Plugins](https://plugins.jenkins.io/)
- [ ]  [Jenkins Tutorial for Beginners](https://www.guru99.com/jenkins-tutorial.html)
