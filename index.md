---
title: Home
nav_order: 1
description: "LPI Exam Study Resources."
permalink: /
---

# LPI Exam Study Resources

Education should never be a financial consideration. The primary goal of this website is to provide you with a completely free library of study resources for all the Linux Professional Institute certification exams. I plan to share my [over 20 years of industry experience](https://www.linkedin.com/in/chinthakadeshapriya/) as a Linux System Engineer, DevOps consultant, Linux Trainer as a Red Hat Certified Instructor, and Red Hat Certified Architect credential with you.

The Linux Professional Institute (LPI) is a global certification organization and career development resource for open source professionals. It is the world's first and largest vendor-neutral Linux and open source certification body, with over 200,000 certification holders. LPI certifies professionals in more than 180 countries, offers exams in multiple languages, and works with hundreds of training partners.

I appreciate the LPI's approach to Linux Distribution Neutral in training and certification. The goal of this website is not to bind you to a specific exam objective, but rather to provide you with enough hands-on industry experience through our industry-oriented lab practices that are included with exam study resources.



